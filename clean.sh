#!/bin/sh

rm -r build/
rm -r dist/
rm -r **/*.egg-info/
rm -r **/*.egg
rm -r **/__pycache__/

