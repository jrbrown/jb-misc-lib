
import unittest
from collections import OrderedDict

from jb_misc_lib.typesafe import *


@TypeCheck(list, int, kw=str, return_type=bool)
def testTypeCheck(*args, **kwargs):
    return True

@TypeCheck(return_type=bool)
def returnBool(x, *args, **kwargs):
    return x


class TestSimpleTypeCheck(unittest.TestCase):

    def testGoodTypes(self):
        testTypeCheck([1,2,3], 5, kw="Hello")

    def testBadArg(self):
        try:
            testTypeCheck([1,2,3], "Some string", kw="Hello")
        except TypeCheckError as e:
            self.assertEqual(e.element, "Some string")
            self.assertEqual(e.valid_types, [int])
        else:
            raise Exception("TypeCheck failed to catch error")

    def testBadKwarg(self):
        try:
            testTypeCheck([1,2,3], 5, kw=5)
        except TypeCheckError as e:
            self.assertEqual(e.element, 5)
            self.assertEqual(e.valid_types, [str])
        else:
            raise Exception("TypeCheck failed to catch error")

    def testBadReturn(self):
        try:
            returnBool(1)
        except TypeCheckError as e:
            self.assertEqual(e.element, 1)
            self.assertEqual(e.valid_types, [bool])
        else:
            raise Exception("TypeCheck failed to catch error")


@GeneralCheck(
    arg_types=OrderedDict({
        "l1": [list, tuple],
        "l2": [list],
        "multiplier": [int]
    }),
    return_types=[int])
def testGeneralTypeCheck(l1, l2, multiplier=1):
    return multiplier * l2[len(l1)]


class TestGeneralTypeCheck(unittest.TestCase):

    def testGoodTypes1(self):
        testGeneralTypeCheck([1,2],[3,4,5])

    def testGoodTypes2(self):
        testGeneralTypeCheck([1,2],l2=[3,4,5],multiplier=3)

    def testGoodTypes3(self):
        testGeneralTypeCheck((1,2),[3,4,5],3)

    def testBadArg(self):
        try:
            testGeneralTypeCheck([1,2],(3,4,5))
        except TypeCheckError as e:
            self.assertEqual(e.element, (3,4,5))
            self.assertEqual(e.valid_types, [list])
        else:
            raise Exception("TypeCheck failed to catch error")

    def testBadKwarg(self):
        try:
            testGeneralTypeCheck([1,2],[3,4,5],multiplier="hello")
        except TypeCheckError as e:
            self.assertEqual(e.element, "hello")
            self.assertEqual(e.valid_types, [int])
        else:
            raise Exception("TypeCheck failed to catch error")

    def testReturn(self):
        try:
            testGeneralTypeCheck([1,2],[3.2,4.1,5.6])
        except TypeCheckError as e:
            self.assertEqual(e.element, 5.6)
            self.assertEqual(e.valid_types, [int])
        else:
            raise Exception("TypeCheck failed to catch error")


@GeneralCheck(
    arg_properties=OrderedDict({
        "l1": ["__len__"],
        "l2": ["__getitem__", "__len__"],
        "multiplier": ["__mul__"]
    }),
    input_conditions=[{"msg": "First list must be smaller than second",
                       "func": lambda l1, l2, *args, **kwargs: len(l1)<len(l2)}],
    output_conditions=[{"msg": "Output must be an integer",
                        "func": lambda out: isinstance(out, int)}])
def testGeneralCheck(l1, l2, multiplier=1):
    return multiplier * l2[len(l1)]


class TestGeneralPropertyCheck(unittest.TestCase):

    def testGoodProperties1(self):
        testGeneralCheck([1,2],[3,4,5])

    def testGoodProperties2(self):
        testGeneralCheck([1,2],l2=[3,4,5],multiplier=3)

    def testGoodProperties3(self):
        testGeneralCheck((1,2),[3,4,5],3)

    def testBadArg(self):
        try:
            testGeneralCheck([1,2],3)
        except PropertyCheckError as e:
            self.assertEqual(e.element, 3)
            self.assertEqual(e.required_property, "__getitem__")
        else:
            raise Exception("TypeCheck failed to catch error")

    def testBadKwarg(self):
        try:
            testGeneralCheck([1,2],[3,4,5],multiplier={"foo": "bar"})
        except PropertyCheckError as e:
            self.assertEqual(e.element, {"foo": "bar"})
            self.assertEqual(e.required_property, "__mul__")
        else:
            raise Exception("TypeCheck failed to catch error")


class TestGeneralConditionCheck(unittest.TestCase):

    def testFailInputCondition(self):
        try:
            testGeneralCheck([1,2,3],[4,5])
        except ConditionCheckError as e:
            self.assertEqual(e.condition_description, "First list must be smaller than second")
            self.assertEqual(e.condition_args, ([1,2,3],[4,5]))
        else:
            raise Exception("TypeCheck failed to catch error")

    def testFailOutputCondition(self):
        try:
            testGeneralCheck([1,2],[3,4,5.5])
        except ConditionCheckError as e:
            self.assertEqual(e.condition_description, "Output must be an integer")
            self.assertEqual(e.condition_args, (5.5,))
        else:
            raise Exception("TypeCheck failed to catch error")


if __name__ == "__main__":
    unittest.main()

